Commerce Discount Usage Statistics Module provides a usage statistics Field and Filter for Views.


INSTALLATION
Requires the commerce_discount_usage module.

Just enable the commerce_discount_usage_stat module.


FIELD
The Field can be added to a Commerce Discount Entity to show the number of times a discount has been used.

Click FIELDS:. Select the `Commerce Discount: Discount Usage statistic` field and Add it to your view.



FILTER
The Filter can be added to a Commerce Discount Entity to filter your view by the number of times a Discount has been used.

Click FILTER CRITERIA:. Select the `Commerce Discount: Discount Usage statistic` field and Apply to your display.

You might want to check the Exposed option in order to see and modify the filter on the view.

Check the Expose operator option to choose an Operator (< | <= | = | != | > | >= | ) to use with the Filter.

 

