<?php

/**
 * @file
 * Provides support for the Views module.
 */

/**
 * Implements hook_views_data_alter().
 */
function commerce_discount_usage_statistics_views_data_alter(&$data) {
  if (isset($data['commerce_discount'])) {

    // Usage stats field.
    $data['commerce_discount']['commerce_discount_usage_stat'] = array(
      'title' => t('Discount Usage statistic'),
      'help' => t('Show discount usage statistic.'),
      'field' => array(
        'handler' => 'commerce_discount_usage_statistics_handler_field_commerce_discount',
      ),
      'filter' => array(
        'handler' => 'commerce_discount_statistics_handler_filter_numeric',
      ),
      'real field' => 'discount_id',
    );

    // Usage relationship.
    $data['commerce_discount']['discount_usage_stat'] = array(
      'relationship' => array(
        'title' => t('Discount usage'),
        'label' => t('Discount usage'),
        'help' => t('Relate this discount to its usage statistics.'),
        'handler' => 'views_handler_relationship',
        'base' => 'commerce_discount_usage_statistics',
        'base field' => 'discount',
        'field' => 'name',
      ),
    );
  }

}
