<?php

/**
 * @file
 * Definition of object.
 *
 * The commerce_discount_statistics_handler_filter_numeric filter.
 */

/**
 * Filter to handle Range.
 *
 * @ingroup views_filter_handlers
 */
class commerce_discount_statistics_handler_filter_numeric extends views_handler_filter_numeric {

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = array(
      '<' => array(
        'title' => t('Is less than'),
        'method' => 'op_less_than',
        'short' => t('<'),
        'values' => 1,
      ),
      '<=' => array(
        'title' => t('Is less than or equal to'),
        'method' => 'op_less_than_or_equal_to',
        'short' => t('<='),
        'values' => 1,
      ),
      '=' => array(
        'title' => t('Is equal to'),
        'method' => 'op_equal_to',
        'short' => t('='),
        'values' => 1,
      ),
      '!=' => array(
        'title' => t('Is not equal to'),
        'method' => 'op_not_equal_to',
        'short' => t('!='),
        'values' => 1,
      ),
      '>=' => array(
        'title' => t('Is greater than or equal to'),
        'method' => 'op_greater_than_or_equal_to',
        'short' => t('>='),
        'values' => 1,
      ),
      '>' => array(
        'title' => t('Is greater than'),
        'method' => 'op_greater_than',
        'short' => t('>'),
        'values' => 1,
      ),
      'between' => array(
        'title' => t('Is between'),
        'method' => 'op_between',
        'short' => t('between'),
        'values' => 2,
      ),
      'not between' => array(
        'title' => t('Is not between'),
        'method' => 'op_not_between',
        'short' => t('not between'),
        'values' => 2,
      ),
    );

    // If the definition allows for the empty operator, add it.
    if (!empty($this->definition['allow empty'])) {
      $operators += array(
        'empty' => array(
          'title' => t('Is empty (NULL)'),
          'method' => 'op_empty',
          'short' => t('empty'),
          'values' => 0,
        ),
        'not empty' => array(
          'title' => t('Is not empty (NOT NULL)'),
          'method' => 'op_not_empty',
          'short' => t('not empty'),
          'values' => 0,
        ),
      );
    }

    // Add regexp support for MySQL.
    if (Database::getConnection()->databaseType() == 'mysql') {
      $operators += array(
        'regular_expression' => array(
          'title' => t('Regular expression'),
          'short' => t('regex'),
          'method' => 'op_regex',
          'values' => 1,
        ),
      );
    }

    return $operators;
  }


  /**
   * Less Than operator.
   */
  public function op_less_than($field) {
    $this->query->add_where_expression(0, "(SELECT COUNT(`discount`) FROM commerce_discount_usage WHERE discount = commerce_discount.name) < :value", array(':value' => $this->value['value']));
  }

  /**
   * Less Than Or Equal To operator.
   */
  public function op_less_than_or_equal_to($field) {
    $this->query->add_where_expression(0, "(SELECT COUNT(`discount`) FROM commerce_discount_usage WHERE discount = commerce_discount.name) <= :value", array(':value' => $this->value['value']));
  }

  /**
   * Equal To operator.
   */
  public function op_equal_to($field) {
    $this->query->add_where_expression(0, "(SELECT COUNT(`discount`) FROM commerce_discount_usage WHERE discount = commerce_discount.name) = :value", array(':value' => $this->value['value']));
  }

  /**
   * Not Equal To operator.
   */
  public function op_not_equal_to($field) {
    $this->query->add_where_expression(0, "(SELECT COUNT(`discount`) FROM commerce_discount_usage WHERE discount = commerce_discount.name) != :value", array(':value' => $this->value['value']));
  }

  /**
   * Greater Than Equal To operator.
   */
  public function op_greater_than_or_equal_to($field) {
    $this->query->add_where_expression(0, "(SELECT COUNT(`discount`) FROM commerce_discount_usage WHERE discount = commerce_discount.name) >= :value", array(':value' => $this->value['value']));
  }

  /**
   * Greater Than operator.
   */
  public function op_greater_than($field) {
    $this->query->add_where_expression(0, "(SELECT COUNT(`discount`) FROM commerce_discount_usage WHERE discount = commerce_discount.name) > :value", array(':value' => $this->value['value']));
  }

  /**
   * Between operator.
   */
  public function op_between($field) {
    $this->query->add_where_expression(0, "(SELECT COUNT(`discount`) FROM commerce_discount_usage WHERE discount = commerce_discount.name) BETWEEN :minimum AND :maximum", array(':minimum' => $this->value['min'], ':maximum' => $this->value['max']));
  }

  /**
   * Between operator.
   */
  public function op_not_between($field) {
    $this->query->add_where_expression(0, "(SELECT COUNT(`discount`) FROM commerce_discount_usage WHERE discount = commerce_discount.name) NOT BETWEEN :minimum AND :maximum", array(':minimum' => $this->value['min'], ':maximum' => $this->value['max']));
  }

  /**
   * Empty operator.
   */
  public function op_empty($field) {
    $this->query->add_where_expression(0, "(SELECT COUNT(`discount`) FROM commerce_discount_usage WHERE discount = commerce_discount.name) = 0", array(':value' => $this->value['value']));
  }

  /**
   * Not Empty Operator.
   */
  public function op_not_empty($field) {
    $this->query->add_where_expression(0, "(SELECT COUNT(`discount`) FROM commerce_discount_usage WHERE discount = commerce_discount.name) != 0", array(':value' => $this->value['value']));
  }

}
